# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyBlog::Application.config.secret_key_base = 'd035850e171cae6834a568d8e9f30b04a412e9375e998579eaf4e1e22cf066333fb43f6db44392ae1a2f7f9d622675fb686fd127f6b20e938da4f52237b0da41'
